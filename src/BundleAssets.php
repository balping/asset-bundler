<?php

/*
    Asset Bundler for Laravel Modules
    Copyright (C) 2017  Balázs Dura-Kovács
    https://github.com/balping

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Balping\AssetBundler;

use Illuminate\Console\Command;
use Nwidart\Modules\Facades\Module;
use MatthiasMullie\PathConverter\Converter as PathConverter;
use Balping\LaravelVersion\LaravelVersion;

class BundleAssets extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'module:bundle-assets';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generates sass and js files bundled from index files in modules';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		if (LaravelVersion::min('5.6')) {
			$modules = collect(Module::allEnabled());
		} else {
			$modules = collect(Module::enabled());
		}

		$sass = "/* This is a generated file. Run artisan module:bundle-assets to update. */\n\n";
		$js = $sass;
		$npmDependencies = [];


		$modules->each(function(\Nwidart\Modules\Module $module) use (&$sass, &$js, &$npmDependencies) {
			$pathConverter = new PathConverter($module->getPath(), resource_path('assets/sass/modules.scss'));
			if(file_exists($module->getPath() . '/Resources/assets/sass/index.scss')){
				$sass .= '@import "' . $pathConverter->convert('Resources/assets/sass/index.scss') . "\";\n";
			}
			unset($pathConverter);

			$pathConverter = new PathConverter($module->getPath(), resource_path('assets/js/modules.js'));
			if(file_exists($module->getPath() . '/Resources/assets/js/index.js')){
				$js .= 'require(\'' . $pathConverter->convert('Resources/assets/js/index.js') . "');\n";
			}
			unset($pathConverter);

			$pathConverter = new PathConverter($module->getPath(), resource_path('installed-modules/package.json'));
			if(file_exists($module->getPath() . '/npm/package.json')){
				$packageJson = json_decode(file_get_contents($module->getPath() . '/npm/package.json'));
				$name = $packageJson->name;
				$npmDependencies[$name] = 'file:' . $pathConverter->convert('npm');
			}
			unset($pathConverter);
		});

		

		$npm = [
			"name"			=> "installed-modules",
			"version"		=> "0.0.1",
			"private"		=> true,
			"dependencies"	=> $npmDependencies
		];

		$npm = json_encode($npm, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

		file_put_contents(resource_path('assets/sass/modules.scss'), $sass);
		file_put_contents(resource_path('assets/js/modules.js'), $js);

		if(!file_exists(resource_path('installed-modules'))) {
			mkdir(resource_path('installed-modules'), 0755, true);
		}

		file_put_contents(resource_path('installed-modules/package.json'), $npm);

		$this->info('modules.scss, modules.js and package.json have been generated.');
	}
}
